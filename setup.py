from setuptools import setup, find_packages

setup(
    name="wpa_supplicant",
    version="0.0.1",
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'wpa_supplicant = wpa_supp.__main__:main'
        ]
    },
)
