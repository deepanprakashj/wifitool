#! /usr/bin/python
"""
This class get bss properties information
"""
from .bss import Bss
from .signature_convert import Signature
from .common import convert
from .aplist import Aplist


class BssProper(Bss):

    """
    Array of object path in bss_proper so handle those
    so set the object path after get property result
    """

    def __init__(self, ifname):
        super(BssProper, self).__init__(ifname)

    def bss_prop_list(self):
        """
        It bss properties
        :return: properties in list
        """
        prop = ['BSSID', 'SSID', 'WPA', 'RSN', 'WPS', 'IEs',
                'Privacy', 'Mode', 'Frequency', 'Rates',
                'Signal', 'Age']
        return prop

    def scan_done(self, success):
        """
        It is scan the available ssid path
        :param success: number scanning
        :return: available ssid
        """
        super(BssProper, self).scan_done(success)
        self.show_ssid()

    def show_ssid(self):
        """
        find ssid in path information
        :return: ssid path
        """
        bss_obj_path_ssid = dict()
        for bss_opath in self.bsss:
            self.wpas_dbus_opath = bss_opath
            self.making_method_calls()
            ssid_byte = self.get_prop("SSID")
            ssid = Signature(ssid_byte).convert_ay_str()
            ssid = "".join(ssid)
            bss_obj_path_ssid[ssid] = bss_opath
        aplist = Aplist(bss_obj_path_ssid)
        aplist.listed()
        self.bss_properties(aplist.path())

    def bss_properties(self, aplist_opath):
        """
        print bss properties information like ssid,bssid...values etc
        :param aplist_opath:path of ssid
        :return: properties values
        """
        self.wpas_dbus_opath = aplist_opath
        self.making_method_calls()
        for prope in self.bss_prop_list():
            val = self.get_prop(prope)
            if prope is 'BSSID':
                print '{} = {}'.format(prope, ''.join(
                    Signature(val).convert_ay_hex()))
            elif prope is 'SSID':
                print '{} = {}'.format(prope, ''.join(
                    Signature(val).convert_ay_str()))
            else:
                print '{} = {}'.format(prope, convert(val))


# if __name__ == "__main__":
def main():
    """
    Provides the BSS's properties, when the user given id from the list
    of available BSSs
    """
    bss_prop = BssProper("wlan0")
    print bss_prop
