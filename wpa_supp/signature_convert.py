#!/usr/bin/python
"""
It is convert the dbus properties to readable formats
"""


class Signature(object):

    """
    Instance to convert the dbus types to python data types.
    """

    def __init__(self, data_type=None):
        self.data_type = data_type

    def convert_ay_str(self):
        """
        convert array of bytes to string
        :return: string array
        """
        return [str(byte) for byte in self.data_type]

    def convert_ay_int(self):
        """
        convert array of byte to int
        :return: int array
        """
        return [int(byte) for byte in self.data_type]

    def convert_ay_hex(self):
        """
        convert array of byte to hex
        :return: It is return a hex array
        """
        return [hex(byte) for byte in self.data_type]
