#!/usr/bin/python
"""
It is get dbus get, set, getall signal information
"""

import dbus
from dbus.mainloop.glib import DBusGMainLoop


class Supplicant(object):

    """
    Get supplicant properties information's
    """

    def __init__(self):
        DBusGMainLoop(set_as_default=True)
        self.bus = dbus.SystemBus()
        self.properties = None
        self.wpas_dbus_name = None
        self.wpas_dbus_interface = None
        self.wpas_dbus_opath = None
        self.proxy = None
        self.iface = None

    def get_proxy(self):
        """
        It is interact with a remote object
        :return: dbus.proxies.ProxyObject
        """
        try:
            self.proxy = self.bus.get_object(
                self.wpas_dbus_name, self.wpas_dbus_opath)
        except dbus.DBusException as ex:
            print ex
        return self.proxy

    def get_iface(self):
        """
        get interface information
        :return: dbus interface it is a string,
        It is slash-separated
        """
        self.iface = dbus.Interface(self.proxy, self.wpas_dbus_interface)
        return self.iface

    def get_prop(self, prop_name):
        """
        Get native APIs properties or attributes information's
        :param prop_name: native API properties name
        :return: dbus properties
        """
        if self.wpas_dbus_interface is not None:
            self.properties = self.proxy.Get(
                self.wpas_dbus_interface,
                prop_name,
                dbus_interface=dbus.PROPERTIES_IFACE)
            return self.properties
        print "Check interface"
